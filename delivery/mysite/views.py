# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponse
from django.template import loader
from django.shortcuts import render

def index(request):
    #return HttpResponse("Olá MySite")
    template = loader.get_template('mysite/index.html')
    context = {
        'data': [],
    }
    return HttpResponse(template.render(context, request))

def contact(request):
    #return HttpResponse("Olá Core")
    template = loader.get_template('mysite/contact.html')
    context = {
        'data': [],
    }
    return HttpResponse(template.render(context, request))