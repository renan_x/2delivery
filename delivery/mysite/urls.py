from django.conf.urls import url

from delivery.mysite import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
]
