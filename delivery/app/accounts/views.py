# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect

from django.core.urlresolvers import reverse, reverse_lazy

from django.views.generic.edit import CreateView, UpdateView, DeleteView

from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader

from models import User
from delivery.app.accounts.forms import UserForm, UserEditForm, UserShowForm, AddressForm, AddressShowForm, UserEditProfile
#from delivery.app.core.forms import AddressForm

from django.contrib import messages

from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth import update_session_auth_hash
#from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

@login_required
def index(request):
	#return HttpResponse("Olá users")
	template = loader.get_template('accounts/users/index.html')
	users = User.objects.all()
	context = {
	'data': [],
	'users': users,
	}
	return HttpResponse(template.render(context, request))


@method_decorator(login_required, name='dispatch')
class CreateUser(CreateView):
	model = User
	template_name = 'accounts/users/form.html'
	form_class = UserForm
	second_form_class = AddressForm
	success_url = reverse_lazy('accounts:users')

	def get(self, request, *args, **kwargs):
		self.object = None
		form = self.form_class
		address_form = self.second_form_class
		return self.render_to_response(
		  self.get_context_data(
			form=form, 
			address_form=address_form
		  )
		)

	def post(self, request, *args, **kwargs):
		self.object = None
		address_form = self.second_form_class(self.request.POST)
		form = self.form_class(self.request.POST)
		
		if form.is_valid():
		  return self.form_valid(form, address_form)
		else:
		  return self.form_invalid(form, address_form)

	def form_valid(self, form, address_form):
		

		provider = address_form.save(commit=False)
		self.object = form.save()
		provider.user = self.object
		provider.save()
		return HttpResponseRedirect(self.get_success_url())

	def form_invalid(self, form, address_form):
		return self.render_to_response(
		  self.get_context_data(
			form=form,
			address_form=address_form
		  )
		)

	def get_success_url(self):
		return reverse_lazy('accounts:users')


@method_decorator(login_required, name='dispatch')
class UpdateUser(UpdateView):
	model = User
	template_name = 'accounts/users/form.html'
	form_class = UserEditForm
	second_form_class = AddressForm
	success_url = reverse_lazy('accounts:users')

	def get(self, request, *args, **kwargs):
		self.object = None
		form = self.form_class
		address_form = self.second_form_class


		return self.render_to_response(
		  self.get_context_data(
			form=form, 
			address_form=address_form
		  )
		)

	def get_context_data(self, **kwargs):
		self.object = self.get_object()
		context = super(UpdateUser, self).get_context_data(**kwargs)
		context['address_form'] = self.second_form_class(instance=self.object.address)
		context['form'] = self.form_class(instance=self.object)

		return context

	def post(self, request, *args, **kwargs):
		
		self.object = self.get_object()	
		
		address_form = self.second_form_class(self.request.POST, instance=self.object.address)
		form = self.form_class(self.request.POST, instance=self.object)

		if form.is_valid():
			print('Valido')
			return self.form_valid(form, address_form)
		else:
			print('inValido')
			return self.form_invalid(form, address_form)

	def form_valid(self, form, address_form):
		form.save()
		address_form.save()

		messages.success(self.request, 'Usuario atualizado com sucesso.')
		return HttpResponseRedirect(reverse('accounts:users'))

	def form_invalid(self, form, address_form):
		return self.render_to_response(
		  self.get_context_data(
			form=form,
			address_form=address_form
		  )
		)

@method_decorator(login_required, name='dispatch')
class ShowUser(UpdateView):
	model = User
	template_name = 'accounts/users/form.html'
	form_class = UserShowForm
	second_form_class = AddressShowForm
	success_url = reverse_lazy('accounts:users')


	def get(self, request, *args, **kwargs):
		self.object = None
		form = self.form_class
		address_form = self.second_form_class

		return self.render_to_response(
		  self.get_context_data(
			form=form, 
			address_form=address_form
		  )
		)


	def get_context_data(self, **kwargs):
		self.object = self.get_object()
		context = super(ShowUser, self).get_context_data(**kwargs)
		context['address_form'] = self.second_form_class(instance=self.object.address)
		context['form'] = self.form_class(instance=self.object)

		return context

@method_decorator(login_required, name='dispatch')
class ShowProfile(UpdateView):
	#return HttpResponse("Olá users")

    model = User

    template_name = 'accounts/profile/index.html'
    form_class = UserEditProfile
    second_form_class = AddressForm
    form_pass = None
    success_url = reverse_lazy('accounts:user_profile')


    def get(self, request, *args, **kwargs):
		self.object = None
		form = self.form_class
		address_form = self.second_form_class
		form_pass = PasswordChangeForm(request.user)
		return self.render_to_response(
		  self.get_context_data(
			form=form, 
			address_form=address_form, 
			form_pass = form_pass 
		  )
		)


    def get_context_data(self, **kwargs):
		self.object = self.get_object()
		context = super(ShowProfile, self).get_context_data(**kwargs)
		context['address_form'] = self.second_form_class(instance=self.object.address)
		context['form'] = self.form_class(instance=self.object)

		return context

    def post(self, request, *args, **kwargs):
		
		self.object = None	
		
		address_form = self.second_form_class(self.request.POST,  instance=self.request.user.address)
		form = self.form_class(self.request.POST, instance=self.request.user)
	
		print(address_form)

		if form.is_valid():
			print('Valido')
			return self.form_valid(form, address_form)
		else:
			print('inValido')
			return self.form_invalid(form, address_form)


    def form_valid(self, form, address_form):
		self.object = form.save(commit=False)
		self.object.user = self.request.user
		self.object.save()

		self.object = address_form.save(commit=False)
		self.object.address = self.request.user.address
		self.object.save()


		messages.success(self.request, 'Usuario atualizado com sucesso.')
		return HttpResponseRedirect(self.get_success_url())


    def form_invalid(self, form, address_form):
		return self.render_to_response(
		  self.get_context_data(
			form=form,
			address_form=address_form
		  )
		)


    def get_object(self):
        return self.request.user

	def get_success_url(self):
		return reverse('accounts:user_profile')

#Atualizar senha em perfil
@login_required
def change_password(request):
    if request.method == 'POST':
        formPass = PasswordChangeForm(request.user, request.POST)
        if formPass.is_valid():
            user = formPass.save()
            update_session_auth_hash(request, user)
            messages.success(request, 'Senha Atualizada com sucesso!')
            return redirect('accounts:user_profile')
        else:
            messages.error(request, 'Não foi possivel atualizar a senha!')
            return redirect('accounts:user_profile')
    else:
    	return redirect('accounts:user_profile')


'''
class ShowUser(UpdateView):
	model = User
	template_name = 'accounts/users/form.html'
	form_class = UserShowForm
	success_url = reverse_lazy('accounts:users')
'''
