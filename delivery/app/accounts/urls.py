from django.conf.urls import url, include
from delivery.app.accounts import views

urlpatterns = [

    url(r'^perfil/$',  views.ShowProfile.as_view(),  name='user_profile'),
	url(r'^password/$', views.change_password, name='change_password'),
    
    url(r'^criar/$',  views.CreateUser.as_view(),  name='user_create'),
    url(r'^edit/(?P<pk>[0-9]+)/$',  views.UpdateUser.as_view(),  name='user_update'),
    url(r'^(?P<pk>[0-9]+)/$',  views.ShowUser.as_view(),  name='user_show'),
    url(r'^',  views.index,  name='users'),


]
