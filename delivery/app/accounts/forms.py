# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms
from models import User, Address
from django.contrib.auth.forms import UserCreationForm

from django.core.exceptions import ValidationError


class UserForm(UserCreationForm):
    
    #USER
    name = forms.CharField(widget=forms.TextInput(attrs={}),label = 'Nome')
    email = forms.CharField(widget=forms.EmailInput(attrs={}), label = 'E-mail')

    is_active = forms.BooleanField(widget=forms.CheckboxInput(attrs={'class':'filled-in'}), label='Ativo')
   
    password1 = forms.CharField(label='Senha', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Confirmar Senha', widget=forms.PasswordInput)


    def clean_email(self):
        email = self.cleaned_data['email'].lower()
        r = User.objects.filter(email=email)
        if r.count():
            raise  ValidationError("Este e-mail ja esta cadastrado!")
        return email


    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        if password1 and password2 and password1 != password2:
            raise ValidationError("As senhas não correspondem!")

        return password2

		
    class Meta:
		model = User
		fields = ['name', 'email', 'is_active', 'password1', 'password2']


class UserEditForm(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput(attrs={}),label = 'Nome')
    email = forms.CharField(widget=forms.EmailInput(attrs={}), label = 'E-mail', disabled=True)
    is_active = forms.BooleanField(widget=forms.CheckboxInput(attrs={'class':'filled-in'}), label='Ativo', required=False)

    class Meta:
        model = User
        fields = ['name', 'email', 'is_active']


class UserShowForm(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput(attrs={}),label = 'Nome', disabled=True)
    email = forms.CharField(widget=forms.EmailInput(attrs={}), label = 'E-mail', disabled=True)
    is_active = forms.BooleanField(widget=forms.CheckboxInput(attrs={'class':'filled-in'}), label='Ativo', disabled=True)


    class Meta:
        model = User
        fields = ['name', 'email', 'is_active']


class UserEditProfile(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput(attrs={}),label = 'Nome')
    email = forms.CharField(widget=forms.EmailInput(attrs={}), label = 'E-mail', disabled=True)
    is_active = forms.BooleanField(widget=forms.CheckboxInput(attrs={'class':'filled-in'}), label='Ativo', required=False, disabled=True)


    class Meta:
        model = User
        fields = ['name', 'email', 'is_active']
    

class AddressForm(forms.ModelForm):

    #Address
    city = forms.CharField(widget=forms.TextInput(attrs={}),label = 'Cidade')
    state = forms.CharField(widget=forms.TextInput(attrs={}), label = 'Estado')

    zip_code = forms.CharField(widget=forms.TextInput(attrs={}), label = 'Cep')
    street = forms.CharField(widget=forms.TextInput(attrs={}), label = 'Rua')
    neighborhood = forms.CharField(widget=forms.TextInput(attrs={}), label = 'Bairro')
    complement = forms.CharField(widget=forms.TextInput(attrs={}), label = 'Complemento')

    class Meta:
        model = Address
        fields = ['state', 'city', 'zip_code', 'street', 'neighborhood', 'complement']

class AddressShowForm(forms.ModelForm):

    #Address
    city = forms.CharField(widget=forms.TextInput(attrs={}),label = 'Cidade', disabled=True)
    state = forms.CharField(widget=forms.TextInput(attrs={}), label = 'Estado', disabled=True)

    zip_code = forms.CharField(widget=forms.TextInput(attrs={}), label = 'Cep', disabled=True)
    street = forms.CharField(widget=forms.TextInput(attrs={}), label = 'Rua', disabled=True)
    neighborhood = forms.CharField(widget=forms.TextInput(attrs={}), label = 'Bairro', disabled=True)
    complement = forms.CharField(widget=forms.TextInput(attrs={}), label = 'Complemento', disabled=True)

    class Meta:
        model = Address
        fields = ['state', 'city', 'zip_code', 'street', 'neighborhood', 'complement']
