# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import re
import uuid

from django.db import models
from django.core import validators
from django.contrib.auth.models import AbstractBaseUser, UserManager, PermissionsMixin, User, BaseUserManager



class User(AbstractBaseUser, PermissionsMixin):

    username = models.CharField('Usuário', default=uuid.uuid4, max_length=200, unique=True, blank=True, null=True)
    name = models.CharField('Nome', max_length=100, blank=True)
    email = models.EmailField('E-mail', unique=True)
    is_active = models.BooleanField('Ativo', default=True)
    #is_admin = models.BooleanField('Administrador', default=False)
    is_staff = models.BooleanField('Equipe', default=False)
    date_joined = models.DateTimeField('Data de Entrada', auto_now_add=True)

    USERNAME_FIELD = 'username'
    EMAIL_FIELD = 'email'
    REQUIRED_FIELDS = ['email']

    objects = UserManager()

class Meta:
    verbose_name = 'Usuário'
    verbose_name_plural = 'Usuários'

    def str(self):
        return self.name or self.username

    def get_full_name(self):
        return str(self)

    def get_short_name(self):
        return str(self).split(" ")[0]

    def delete(self):
        return super(User, self).delete()


class Address(models.Model):
    zip_code = models.CharField('CEP', max_length=10)
    city = models.CharField('Cidade', max_length=255)
    state = models.CharField('UF', max_length=2)
    street = models.CharField('Rua', max_length=255)
    number = models.CharField('Número', max_length=20)
    neighborhood = models.CharField('Bairro', max_length=255)
    complement = models.CharField(
        'Complemento', max_length=255, blank=True, null=True)
    reference_point = models.CharField(
        'Ponto de Referência', max_length=255, blank=True, null=True)
    country = models.CharField('País', max_length=255, default='Brasil')

    # Generic Relation
    #content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    #object_id = models.PositiveIntegerField(db_index=True)
    #content_object = GenericForeignKey('content_type', 'object_id')

    user = models.OneToOneField(User)

    class Meta:
        verbose_name = 'Endereço'
        verbose_name_plural = 'Endereços'
        ordering = ['zip_code']