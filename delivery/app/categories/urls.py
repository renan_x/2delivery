from django.conf.urls import url, include
from django.contrib import admin

from delivery.app.categories import views



urlpatterns = [
    url(r'^criar/$',  views.CreateCategories.as_view(),  name='CreateCategories'),
    url(r'^atualizar/(?P<pk>[0-9]+)/$',  views.UpdateCategories.as_view(),  name='UpdateCategories'),
    url(r'^(?P<pk>[0-9]+)/$',  views.ShowCategories.as_view(),  name='ShowCategories'),
    url(r'^',  views.index,  name='categories'),
   

]
