# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms
from models import Categories
#from django.contrib.auth.forms import ProductsCreationForm

#from django.core.exceptions import ValidationError


class CategoriesForm(forms.ModelForm):
    '''
    produto = forms.CharField(widget=forms.TextInput(attrs={}),label = 'Nome do produto')
    descricao_produto = forms.CharField(widget=forms.TextInput(attrs={}), label = 'Descrição do Produto')
    quantidade_estoque = forms.CharField( widget=forms.IntegerField(),label='Quantidade em Estoque')
    preco = forms.CharField(widget=forms.DecimalField(), label='Preço')
    imagem = forms.CharField(widget=forms.TextInput(attrs={}),label = 'Imagem do Produto')
    '''
    class Meta:
        model = Categories
        fields = ['categoria', 'descricao']

'''
class ProductsEditForm(forms.ModelForm):
    produto = forms.CharField(widget=forms.TextInput(attrs={}), label='Nome do produto')
    descricao_produto = forms.CharField(widget=forms.CharField(attrs={}), label='Descrição do Produto')
    quantidade_estoque = forms.CharField(widget=forms.IntegerField, label='Quantidade em Estoque')
    preco = forms.DecimalField(widget=forms.DecimalField, label='Preço')
    imagem = forms.CharField(widget=forms.TextInput(attrs={}), label='Imagem do Produto')

    class Meta:
        model = Products
        fields = ['produto', 'descricao_produto', 'preco', 'quantidade_estoque', 'imagem']
'''

class CategoriesShowForm(forms.ModelForm):
    categoria = forms.CharField(widget=forms.TextInput(attrs={}), label='Categoria',disabled=True)
    descricao = forms.CharField(widget=forms.TextInput(attrs={}), label='Descrição',disabled=True)

    class Meta:
        model = Categories
        fields = ['categoria', 'descricao']