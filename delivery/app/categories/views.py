# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect

from django.core.urlresolvers import reverse, reverse_lazy

from django.views.generic.edit import CreateView, UpdateView, DeleteView

from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader

from models import Categories
from delivery.app.categories.forms import CategoriesForm, CategoriesShowForm

from django.contrib import messages


#from django.contrib.auth.forms import UserCreationForm


def index(request):
    template = loader.get_template('categories/index.html')
    categories = Categories.objects.all()
    context = {
        'data': [],
        'categories': categories,
    }
    return HttpResponse(template.render(context, request))
'''
def product_create(request):
    template = loader.get_template('products/prod_create.html')
    context = {
        'data': [],
    }

    return HttpResponse(template.render(context, request))
'''

class CreateCategories(CreateView):
    model = Categories
    template_name = 'categories/form.html'
    form_class = CategoriesForm
    success_url = reverse_lazy('categories:categories')
    '''
    def get(self, request, *args, **kwargs):
        self.object = None
        form = self.form_class
        return self.render_to_response(
            self.get_context_data(
                form=form
            )
        )

    def post(self, request, *args, **kwargs):
        self.object = None
        form = self.form_class(self.request.POST)

        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):

        self.object = form.save()
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form):
        return self.render_to_response(
            self.get_context_data(
                form=form
            )
        )

    def get_success_url(self):
        return reverse_lazy('accounts:products')
    '''

class UpdateCategories(UpdateView):
    model = Categories
    template_name = 'categories/form.html'
    form_class = CategoriesForm
    success_url = reverse_lazy('categories:categories')

'''
    def get(self, request, *args, **kwargs):
        self.object = None
        form = self.form_class
        address_form = self.second_form_class

        return self.render_to_response(
            self.get_context_data(
                form=form,
                address_form=address_form
            )
        )

    def get_context_data(self, **kwargs):
        self.object = self.get_object()
        context = super(UpdateUser, self).get_context_data(**kwargs)
        context['address_form'] = self.second_form_class(instance=self.object.address)
        context['form'] = self.form_class(instance=self.object)

        return context

    def post(self, request, *args, **kwargs):

        self.object = self.get_object()

        address_form = self.second_form_class(self.request.POST, instance=self.object.address)
        form = self.form_class(self.request.POST, instance=self.object)

        if form.is_valid():
            print('Valido')
            return self.form_valid(form, address_form)
        else:
            print('inValido')
            return self.form_invalid(form, address_form)

    def form_valid(self, form, address_form):
        form.save()
        address_form.save()

        messages.success(self.request, 'Usuario atualizado com sucesso.')
        return HttpResponseRedirect(reverse('accounts:users'))

    def form_invalid(self, form, address_form):
        return self.render_to_response(
            self.get_context_data(
                form=form,
                address_form=address_form
            )
        )
    '''

class ShowCategories(UpdateView):
    model = Categories
    template_name = 'categories/form.html'
    form_class = CategoriesShowForm
    success_url = reverse_lazy('categories:categories')

    '''
    def get(self, request, *args, **kwargs):
        self.object = None
        form = self.form_class
        address_form = self.second_form_class

        return self.render_to_response(
            self.get_context_data(
                form=form,
                address_form=address_form
            )
        )

    def get_context_data(self, **kwargs):
        self.object = self.get_object()
        context = super(ShowUser, self).get_context_data(**kwargs)
        context['address_form'] = self.second_form_class(instance=self.object.address)
        context['form'] = self.form_class(instance=self.object)

        return context

    '''

'''
def list_products(request):
	products = Products.objects.all()
	return render(request,'products.html',{'products': products})
'''