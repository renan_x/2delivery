# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class Categories(models.Model):
    categoria = models.CharField('Categoria', max_length=100)
    descricao = models.CharField('Descrição', max_length=500)

    def __str__(self):
        return self.categoria
