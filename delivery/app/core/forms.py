# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms

from delivery.app.core.models import Address
from django.core.exceptions import ValidationError


class AddressForm(forms.ModelForm):

    #Address
    city = forms.CharField(widget=forms.TextInput(attrs={}),label = 'Cidade')
    state = forms.CharField(widget=forms.EmailInput(attrs={}), label = 'Estado')

    class Meta:
        model = Address
        fields = ['state', 'city']



    