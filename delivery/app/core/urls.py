"""delivery URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from delivery.app.core import views

urlpatterns = [
    url(r'^usuarios/', include('delivery.app.accounts.urls', namespace='accounts')),
    url(r'^produtos/', include('delivery.app.products.urls', namespace='products')),
    url(r'^categorias/', include('delivery.app.categories.urls', namespace='categories')),
    url(r'^promocoes/', include('delivery.app.promotions.urls', namespace='promotions')),
    url(r'^clientes/', include('delivery.app.clients.urls', namespace='clients')),
    url(r'^pedidos/', include('delivery.app.requests.urls', namespace='requests')),

    url(r'^help/',  views.help, name='help'),
    url(r'^',  views.index, name='dashboard'),

]
