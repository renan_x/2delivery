# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponse
from django.template import loader
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from delivery.app.clients.models import Clients
from delivery.app.products.models import Products

@login_required
def index(request):
    #return HttpResponse("Olá Core")
    template = loader.get_template('core/index.html')
    context = {
        'data': [],
        'clients': Clients.objects.count(),
        'products' : Products.objects.count(),
    }
    #return HttpResponse(Clients.objects.count())
    return HttpResponse(template.render(context, request))

def help(request):
    #return HttpResponse("Olá Core")
    template = loader.get_template('core/help.html')
    context = {
        'data': [],
    }
    return HttpResponse(template.render(context, request))
