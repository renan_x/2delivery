# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms
from models import Products
#from django.contrib.auth.forms import ProductsCreationForm

#from django.core.exceptions import ValidationError ../categories/{% url 'categories:IndexCategories' %}


class ProductsForm(forms.ModelForm):
    '''
    produto = forms.CharField(widget=forms.TextInput(attrs={}),label = 'Nome do produto')
    descricao_produto = forms.CharField(widget=forms.TextInput(attrs={}), label = 'Descrição do Produto')
    quantidade_estoque = forms.CharField( widget=forms.IntegerField(),label='Quantidade em Estoque')
    preco = forms.CharField(widget=forms.DecimalField(), label='Preço')
    imagem = forms.CharField(widget=forms.TextInput(attrs={}),label = 'Imagem do Produto')
    '''
    class Meta:
        model = Products
        fields = ['produto', 'descricao_produto', 'preco', 'quantidade_estoque', 'imagem', 'categoria']

'''
class ProductsEditForm(forms.ModelForm):
    produto = forms.CharField(widget=forms.TextInput(attrs={}), label='Nome do produto')
    descricao_produto = forms.CharField(widget=forms.CharField(attrs={}), label='Descrição do Produto')
    quantidade_estoque = forms.CharField(widget=forms.IntegerField, label='Quantidade em Estoque')
    preco = forms.DecimalField(widget=forms.DecimalField, label='Preço')
    imagem = forms.CharField(widget=forms.TextInput(attrs={}), label='Imagem do Produto')

    class Meta:
        model = Products
        fields = ['produto', 'descricao_produto', 'preco', 'quantidade_estoque', 'imagem']
'''

class ProductsShowForm(forms.ModelForm):

    produto = forms.CharField(widget=forms.TextInput(attrs={}), label='Nome do produto',disabled=True)
    descricao_produto = forms.CharField(widget=forms.TextInput(attrs={}), label='Descrição do produto',disabled=True)
    quantidade_estoque = forms.CharField(disabled=True)
    preco = forms.CharField(disabled=True)

    class Meta:
        model = Products
        fields = ['produto', 'descricao_produto', 'preco', 'quantidade_estoque', 'imagem', 'categoria']