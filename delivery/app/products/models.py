# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from delivery.app.categories.models import Categories

class Products(models.Model):
    produto = models.CharField('Produto',max_length=100)
    descricao_produto = models.CharField('Descrição do Produto',max_length=500)
    quantidade_estoque = models.IntegerField('Quantidade em Estoque')
    preco = models.DecimalField('Preço',max_digits=5,decimal_places=2)
    imagem = models.ImageField("Imagem", upload_to='images/products')
    
    categoria = models.ForeignKey(Categories, default="", on_delete=models.CASCADE)

    def __str__(self):
        return self.descricao_produto
