from django.conf.urls import url, include
from django.contrib import admin

from delivery.app.products import views



urlpatterns = [
    url(r'^criar/$',  views.CreateProduct.as_view(),  name='CreateProduct'),
    url(r'^atualizar/(?P<pk>[0-9]+)/$',  views.UpdateProduct.as_view(),  name='UpdateProduct'),
    url(r'^(?P<pk>[0-9]+)/$',  views.ShowProduct.as_view(),  name='ShowProduct'),
    url(r'^',  views.index,  name='products'),
   

]
