from django.conf.urls import url, include
from django.contrib import admin

from delivery.app.promotions import views

urlpatterns = [
    url(r'^criar/$',  views.CreatePromotions.as_view(),  name='CreatePromotions'),
    url(r'^atualizar/(?P<pk>[0-9]+)/$',  views.UpdatePromotions.as_view(),  name='UpdatePromotions'),
    url(r'^(?P<pk>[0-9]+)/$',  views.ShowPromotions.as_view(),  name='ShowPromotions'),
    url(r'^$',  views.index,  name='promotions'),
]