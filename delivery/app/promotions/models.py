# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from delivery.app.products.models import Products

class Promotions(models.Model):
	
	promocao = models.CharField('Promoção', max_length=150)
	descricao_promocao = models.CharField('Descrição da Promoção', max_length=500)
	preco = models.DecimalField('Preço', max_digits=5, decimal_places=2)
	imagem = models.ImageField("Imagem", upload_to='images/promotions')
	produto = models.OneToOneField(Products, default="")

	def __str__(self):
		return (self.promocao)