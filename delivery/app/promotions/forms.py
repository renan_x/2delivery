# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django import forms
from models import Promotions


class PromotionsForm(forms.ModelForm):
    """
    produto = forms.CharField(widget=forms.TextInput(attrs={}),label = 'Nome do produto')
    descricao_produto = forms.CharField(widget=forms.TextInput(attrs={}), label = 'Descrição do Produto')
    quantidade_estoque = forms.CharField( widget=forms.IntegerField(),label='Quantidade em Estoque')
    preco = forms.CharField(widget=forms.DecimalField(), label='Preço')
    imagem = forms.CharField(widget=forms.TextInput(attrs={}),label = 'Imagem do Produto')
    """
    class Meta:
        model = Promotions
        fields = [ 'promocao', 'descricao_promocao', 'preco', 'imagem', 'produto']

'''
class ProductsEditForm(forms.ModelForm):
    produto = forms.CharField(widget=forms.TextInput(attrs={}), label='Nome do produto')
    descricao_produto = forms.CharField(widget=forms.CharField(attrs={}), label='Descrição do Produto')
    quantidade_estoque = forms.CharField(widget=forms.IntegerField, label='Quantidade em Estoque')
    preco = forms.DecimalField(widget=forms.DecimalField, label='Preço')
    imagem = forms.CharField(widget=forms.TextInput(attrs={}), label='Imagem do Produto')

    class Meta:
        model = Products
        fields = ['produto', 'descricao_produto', 'preco', 'quantidade_estoque', 'imagem']
'''

class PromotionsShowForm(forms.ModelForm):

    promocao = forms.CharField(widget=forms.TextInput(attrs={}), label='Nome da promocao',disabled=True)
    descricao_promocao = forms.CharField(widget=forms.TextInput(attrs={}), label='Descrição da promocao',disabled=True)
    preco = forms.CharField(disabled=True)

    class Meta:
        model = Promotions
        fields = ['promocao', 'descricao_promocao', 'preco', 'imagem', 'produto']