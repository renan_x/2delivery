# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect

from django.core.urlresolvers import reverse, reverse_lazy

from django.views.generic.edit import CreateView, UpdateView, DeleteView

from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader

from models import Requests
from delivery.app.requests.forms import RequestsForm, RequestsShowForm

from django.contrib import messages


#from django.contrib.auth.forms import UserCreationForm


def index(request):
    template = loader.get_template('requests/index.html')
    requests = Requests.objects.all()
    context = {
        'data': [],
        'requests': requests,
    }
    return HttpResponse(template.render(context, request))


class CreateRequest(CreateView):
    model = Requests
    template_name = 'requests/form.html'
    form_class = RequestsForm
    success_url = reverse_lazy('requests:requests')


class UpdateRequest(UpdateView):
    model = Requests
    template_name = 'requests/form.html'
    form_class = RequestsForm
    success_url = reverse_lazy('requests:requests')


class ShowRequest(UpdateView):
    model = Requests
    template_name = 'requests/form.html'
    form_class = RequestsShowForm
    success_url = reverse_lazy('requests:requests')

