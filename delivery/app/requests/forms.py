# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms
from models import Requests

from delivery.app.products.models import Products
from delivery.app.clients.models import Clients
#from django.contrib.auth.forms import ProductsCreationForm

#from django.core.exceptions import ValidationError
Options = [
        (1, 'Pendente'),
        (2, 'Entregue'),
        (3, 'Acaminho'),
        (4, 'Cancelado'),
      ]

class RequestsForm(forms.ModelForm):
    descricao = forms.CharField(widget=forms.TextInput(attrs={}), label='Descrição do perido')
    preco = forms.CharField(widget=forms.TextInput(attrs={}), label='Preço do pedido')
    data = forms.CharField(widget=forms.TextInput(attrs={}), label='Data')
    #cliente = forms.CharField(widget=forms.Select(attrs={}), label='Cliente')
    #produtos = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple(attrs={ 'multiple':'multiple'}), label='Produtos')
    
    status = forms.ChoiceField(widget=forms.Select(attrs={}), label='Cliente', choices = Options)

    class Meta:
        model = Requests
        fields = ['descricao', 'preco', 'data', 'cliente', 'status', 'produtos']



class RequestsShowForm(forms.ModelForm):
    descricao = forms.CharField(widget=forms.TextInput(attrs={}), label='Descrição do perido',disabled=True)
    preco = forms.CharField(widget=forms.TextInput(attrs={}), label='Preço do pedido',disabled=True)
    data = forms.CharField(widget=forms.TextInput(attrs={}), label='Data',disabled=True)
    #cliente = forms.ChoiceField(widget=forms.Select(attrs={}), label='Cliente',disabled=True)
    #produtos = forms.CharField(widget=forms.Select(attrs={}), label='Produtos',disabled=True)

    #produtos = forms.MultipleChoiceField(widget=forms.SelectMultiple(), label='Produtos',disabled=True)
    
    cliente = forms.ModelChoiceField(widget=forms.Select(attrs={}), label='Cliente', queryset = Clients.objects.all(),disabled=True)  
    #produtos = forms.ModelChoiceField(widget=forms.SelectMultiple(attrs={}), label='Produtos', queryset = Products.objects.all(), disabled=True)  
    #cliente = forms.ChoiceField(widget=forms.Select(attrs={}), label='Cliente', choices=[
    #(item.pk, item.cliente) for item in Clients.objects.all()])
    #produtos = forms.SelectMultiple(attrs={'multiple':'multiple'} )
    
    #produtos = forms.ChoiceField(widget=forms.SelectMultiple(attrs={'multiple':'multiple'}))
    
    status = forms.ChoiceField(widget=forms.Select(attrs={}), label='Cliente', choices = Options, disabled=True)


    class Meta:
        model = Requests
        fields = ['descricao', 'preco', 'data', 'cliente', 'status', 'produtos']