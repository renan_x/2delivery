from django.conf.urls import url, include
from django.contrib import admin

from delivery.app.requests import views

urlpatterns = [
    url(r'^criar/$', views.CreateRequest.as_view(), name='CreateRequest'),
    url(r'^atualizar/(?P<pk>[0-9]+)/$', views.UpdateRequest.as_view(), name='UpdateRequest'),
    url(r'^(?P<pk>[0-9]+)/$', views.ShowRequest.as_view(), name='ShowRequest'),
    url(r'^', views.index, name='requests'),

]