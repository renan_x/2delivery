# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from delivery.app.clients.models import Clients
from delivery.app.products.models import Products

# Create your models here.
class Requests(models.Model):
    descricao = models.CharField('Descrição', max_length=500)
    preco = models.DecimalField('Preço', max_digits=5, decimal_places=2)
    data = models.CharField('data', max_length=500)

    status = models.IntegerField('status')

    cliente = models.OneToOneField(Clients)
    produtos = models.ManyToManyField(Products)

    def __str__(self):
        return self.descricao