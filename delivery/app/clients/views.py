# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect

from django.core.urlresolvers import reverse, reverse_lazy

from django.views.generic.edit import CreateView, UpdateView, DeleteView

from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader

from models import Clients
from delivery.app.clients.forms import ClientsForm, ClientsShowForm

from django.contrib import messages


#from django.contrib.auth.forms import UserCreationForm


def index(request):
    template = loader.get_template('clients/index.html')
    clients = Clients.objects.all()
    context = {
        'data': [],
        'clients': clients,
    }
    return HttpResponse(template.render(context, request))


class CreateClient(CreateView):
    model = Clients
    template_name = 'clients/form.html'
    form_class = ClientsForm
    success_url = reverse_lazy('clients:clients')


class UpdateClient(UpdateView):
    model = Clients
    template_name = 'clients/form.html'
    form_class = ClientsForm
    success_url = reverse_lazy('clients:clients')


class ShowClient(UpdateView):
    model = Clients
    template_name = 'clients/form.html'
    form_class = ClientsShowForm
    success_url = reverse_lazy('clients:clients')

