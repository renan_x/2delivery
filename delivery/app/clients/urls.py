from django.conf.urls import url, include
from django.contrib import admin

from delivery.app.clients import views

urlpatterns = [
    url(r'^criar/$', views.CreateClient.as_view(), name='CreateClient'),
    url(r'^atualizar/(?P<pk>[0-9]+)/$', views.UpdateClient.as_view(), name='UpdateClient'),
    url(r'^(?P<pk>[0-9]+)/$', views.ShowClient.as_view(), name='ShowClient'),
    url(r'^', views.index, name='clients'),

]