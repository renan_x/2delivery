# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class Clients(models.Model):
    cliente = models.CharField('Nome',max_length=100)
    email = models.CharField('Email',max_length=100)
    cpf = models.CharField('CPF',max_length=100)
    phone = models.CharField('Phone',max_length=100)
    senha = models.CharField('Endereço',max_length=100)

    def __str__(self):
        return self.email
