# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms
from models import Clients
#from django.contrib.auth.forms import ProductsCreationForm

#from django.core.exceptions import ValidationError


class ClientsForm(forms.ModelForm):

    class Meta:
        model = Clients
        fields = ['cliente', 'email', 'cpf', 'phone', 'senha']



class ClientsShowForm(forms.ModelForm):

    cliente = forms.CharField(widget=forms.TextInput(attrs={}), label='Nome do cliente',disabled=True)
    email = forms.CharField(widget=forms.TextInput(attrs={}), label='E-mail do cliente',disabled=True)
    cpf = forms.CharField(widget=forms.TextInput(attrs={}), label='CPF',disabled=True)
    phone = forms.CharField(widget=forms.TextInput(attrs={}), label='Telefone',disabled=True)
    senha = forms.CharField(widget=forms.TextInput(attrs={}), label='Senha',disabled=True)

    class Meta:
        model = Clients
        fields = ['cliente', 'email', 'cpf', 'phone', 'senha']